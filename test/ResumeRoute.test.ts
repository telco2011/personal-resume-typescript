import * as mocha from 'mocha';
import * as chai from 'chai';
import chaiHttp = require('chai-http');

import app from '../src/app';

let resume = require('../src/data/resume.json');

chai.use(chaiHttp);
const expect = chai.expect;

describe('Resume Service', function(){

	//GET Personal Information 
	it('get personal information', () =>{
				return chai.request(app).get('/api/v1/personal-info')
					.then(res => {
						expect(res.status).to.equals(200);
						expect(res).to.be.json;
						expect(res.body).to.be.an('object');
						expect(res.body).to.deep.equal(resume.personalInfo);
					});

	});

	//GET Career Profile 
	xit('get career profile', () =>{
				return chai.request(app).get('/api/v1/careerProfile')
					.then(res => {
						expect(res.status).to.equals(200);
						expect(res).to.be.html;
						expect(res.body).to.be.an('object');
						expect(res.body).to.deep.equal(resume.careerProfile);
					});

	});

	//GET Experiencies 
	it('get experiences', () =>{
				return chai.request(app).get('/api/v1/experiences')
					.then(res => {
						expect(res.status).to.equals(200);
						expect(res).to.be.json;
						expect(res.body).to.be.an('array');
						expect(res.body).to.deep.equal(resume.experiencies);
					});

	});

	//GET Education
	it('get education', () =>{
				return chai.request(app).get('/api/v1/education')
					.then(res => {
						expect(res.status).to.equals(200);
						expect(res).to.be.json;
						expect(res.body).to.be.an('array');
						expect(res.body).to.deep.equal(resume.personalInfo.education);
					});

	});

	//GET Additional Information 
	it('get additional information', () =>{
				return chai.request(app).get('/api/v1/additionalInfo')
					.then(res => {
						expect(res.status).to.equals(200);
						expect(res).to.be.json;
						expect(res.body).to.be.an('object');
						expect(res.body).to.deep.equal(resume.additionalInfo);
					});

	});

	//GET Projects
	it('get projects', () =>{
				return chai.request(app).get('/api/v1/projects')
					.then(res => {
						expect(res.status).to.equals(200);
						expect(res).to.be.json;
						expect(res.body).to.be.an('array');
						expect(res.body).to.deep.equal(resume.projects);
					});

	});

	//GET Skills
	it('get skills', () =>{
				return chai.request(app).get('/api/v1/skills')
					.then(res => {
						expect(res.status).to.equals(200);
						expect(res).to.be.json;
						expect(res.body).to.be.an('array');
						expect(res.body).to.deep.equal(resume.skills);
					});

	});

});


