
/*  Express Web Application - REST API Host  */
import * as path from 'path';
import * as express from 'express';
import * as logger from 'morgan';
import * as bodyParser from 'body-parser';

import ResumeRouter from './routes/ResumeRoute';

import { HTTPStatus } from './routes/DefaultRouter';

import { factory } from './config/LoggerConfig';

const log = factory.getLogger('app');

class App {

	public express: express.Application;
	
	constructor() {
		this.express = express();
		this.middleware();
		this.routesv1();
	}
	
	private middleware(): void {
		// configure pug
		this.express.set("views", path.join(__dirname, "views"));
		this.express.set("view engine", "pug");

		// configure expressjs
		this.express.use(logger('combined'));
		this.express.use(bodyParser.json());
		this.express.use(bodyParser.urlencoded({extended: false}));

		// AllowCORS
		this.express.use(this.AllowCORS);

		// response for other urls
		//this.express.use('/*', this.URLNotFound);
	}

	private AllowCORS (req, res, next) {
		res.header("Access-Control-Allow-Origin", "*");
		res.header("Access-Control-Allow-Headers", "Origin, X-Requested-With, Content-Type, Accept");
		next();
	}

	private URLNotFound(req, res) {
		let url = req.originalUrl;
		let msg = 'Url ' + url + ' not found.'; 
		
		log.warn(msg);
		res.status(HTTPStatus.NOT_FOUND);
		res.render("index", { title: "APP", message: msg });
	}

	private routesv1(): void {
		this.express.use('/api/v1', ResumeRouter);
	}

}

export default new App().express;