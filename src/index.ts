import * as http from 'http';
import * as debug from 'debug';

import { factory } from './config/LoggerConfig';

import App from './app';

const log = factory.getLogger('index');

debug('ts-express:server');

const port = normalizePort(process.env.PORT || 9000);
App.set('port', port);

const server = http.createServer(App);
server.listen(port);
server.on('error', onError);
server.on('listening', onListening);

function normalizePort(val: number|string): number|string|boolean {

	let port: number = (typeof val === 'string') ? parseInt(val,10): val;

	if(isNaN(port)) return val;
	else if(port > 0 ) return port;
	else return false;

}

function onError(error: NodeJS.ErrnoException): void {

  if (error.syscall !== 'listen') throw error;
  let bind = (typeof port === 'string') ? 'Pipe ' + port : 'Port ' + port;
  switch(error.code) {
    case 'EACCES':
      log.error(`${bind} requires elevated privileges`);
      process.exit(1);
      break;
    case 'EADDRINUSE':
      log.error(`${bind} is already in use`);
      process.exit(1);
      break;
    default:
      log.error(error.message);
      throw error;
  }
  
}

function onListening(): void {
  let addr = server.address();
  let bind = (typeof addr === 'string') ? `pipe ${addr}` : `port ${addr.port}`;
  debug(`Listening on ${bind}`);
  log.info(`Listening on ${bind}`);
}