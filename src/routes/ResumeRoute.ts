import { Router, Request, Response, NextFunction} from 'express';
import { DefaultRouter, HTTPStatus } from './DefaultRouter';

import { factory } from '../config/LoggerConfig';

import { PersonalInfo, ExperienceDef, AdditionalInfo, Projects, Education } from 'resume-schema-to-typescript';

let resume = require('../data/resume.json');
const log = factory.getLogger('ResumeRouter');

export class ResumeRouter extends DefaultRouter {

	public GetPersonalInfo(req:Request, res: Response) {

        res.status(HTTPStatus.OK).send(<PersonalInfo> resume.personalInfo);

	}

    public GetCareerProfile(req:Request, res: Response) {

        res.status(HTTPStatus.OK).send(<String> resume.careerProfile);

	}

    public GetExperiences(req:Request, res: Response) {

        res.status(HTTPStatus.OK).send(<Array<ExperienceDef>> resume.experiencies);

	}

    public GetEducation(req:Request, res: Response) {

        res.status(HTTPStatus.OK).send(<Array<Education>> resume.personalInfo.education);

	}

    public GetAdditionalInfo(req:Request, res: Response) {

        res.status(HTTPStatus.OK).send(<AdditionalInfo> resume.additionalInfo);

	}

    public GetProjects(req:Request, res: Response) {

        res.status(HTTPStatus.OK).send(<Array<Projects>> resume.projects);

	}

    public GetSkills(req:Request, res: Response) {

        res.status(HTTPStatus.OK).send(<Array<Object>> resume.skills);

	}

	init() {
		this.router.get("/personal-info", this.GetPersonalInfo);
        this.router.get("/careerProfile", this.GetCareerProfile);
        this.router.get("/experiences", this.GetExperiences);
        this.router.get("/education", this.GetEducation);
        this.router.get("/additionalInfo", this.GetAdditionalInfo);
        this.router.get("/projects", this.GetProjects);
        this.router.get("/skills", this.GetSkills);
	}

}

const resumeRouter = new ResumeRouter();
resumeRouter.init();

export default resumeRouter.router;